//
//  SettingsViewController.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 8.04.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import UIKit
import FirebaseAuth

class SettingsViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && indexPath.row == 0 {
            try! Auth.auth().signOut()
            UserDefaults.standard.set(false, forKey: "userSignedIn")
            UserDefaults.standard.set(" ", forKey: "userID")
            self.dismiss(animated: true, completion: nil)
        }
    }
}
