//
//  UserCell.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 22.04.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {

    @IBOutlet weak var profilePhotoImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
}
