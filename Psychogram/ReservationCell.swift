//
//  ReservationCell.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 15.05.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import UIKit
import Firebase

class ReservationCell: UITableViewCell {

    
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var profilePhotoIV: UIImageView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    let meetingRef = Database.database().reference(withPath: "meetings")
    var requesterId: String?
    var reservationKey: String?
    @IBOutlet weak var acceptReservation: UIButton!
    
    @IBAction func createMeeting(_ sender: Any) {

        meetingRef.observeSingleEvent(of: .value, with: {snapshot in
            if !snapshot.hasChild("\(self.requesterId!)_\(LoginModel.loginUser.userId)") {
                let now = Date()
                let dateFormatter = ISO8601DateFormatter()
                let time = dateFormatter.string(from: now)
                let meeting = [
                    "doctorId": "\(LoginModel.loginUser.userId)",
                    "lastMessage": [
                        "message" : "Rezervasyonunuz onaylandı.",
                        "date": "\(time)",
                        "senderId" : "\(LoginModel.loginUser.userId)"
                    ],
                    "userId" : "\(self.requesterId!)"
                ] as [String : Any]
                
                let newMeetingChild = self.meetingRef.child("\(self.requesterId!)_\(LoginModel.loginUser.userId)")
                newMeetingChild.setValue(meeting)
            }
            
            let reservationsOfMeeting = self.meetingRef.child("\(self.requesterId!)_\(LoginModel.loginUser.userId)/reservations")
            let child = reservationsOfMeeting.childByAutoId()
            child.setValue(self.reservationKey!)
            
            self.acceptButton.backgroundColor = .systemGreen
            self.acceptButton.setTitle("Onayladın", for: .normal)
        })
    }
}
