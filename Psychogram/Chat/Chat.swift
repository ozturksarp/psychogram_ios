//
//  Chat.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 26.04.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import Foundation
import Firebase

struct Chat {
    var doctorId: String!
    var userId: String!
    var lastMessage: Message!
    var reservationKeys = [String]()
}

extension Chat {
    
    init?(dictionary: [String: Any]) {
        guard let doctor = dictionary["doctorId"] as? String,
            let user = dictionary["userId"] as? String else {return nil}
        self.init(doctorId: doctor, userId: user)
    }
    
    init?(snapshot: DataSnapshot) {
        guard
          let value = snapshot.value as? [String: AnyObject],
          let doctor = value["doctorId"] as? String,
          let reservations = value["reservations"] as? [String: String], // ya yoksa?
          let last = value["lastMessage"] as? [String: AnyObject],
          let patient = value["userId"] as? String else {
          return nil
        }
        
        lastMessage = Message(dictionary: last)
        
        for (_, value) in reservations {
            reservationKeys.append(value)
        }
        
        self.doctorId = doctor
        self.userId = patient
    }
}
