//
//  ChatViewController.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 26.04.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import UIKit
import MessageKit
import InputBarAccessoryView
import Firebase

class ChatViewController: MessagesViewController {
    
    var messages: [Message] = []
    var otherUserId: String!
    var chatRoom: Chat!
    var reference = Database.database().reference(withPath: "reservations")
    var currentRes = ""
    var dateFormatter = ISO8601DateFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messageInputBar.delegate = self
        messageInputBar.inputTextView.placeholder = "Mesaj Yolla"
        
        
        
        if !(chatRoom.reservationKeys.isEmpty) {
            for res in chatRoom.reservationKeys {
                reference = Database.database().reference(withPath: "reservations/\(res)")
                reference.observeSingleEvent(of: .value, with: { snap in
                    let currentRes = ReservationModel(snapshot: snap)
                    var now = Date()
                    now = now.addingTimeInterval(3600*3)
                    guard var start = self.dateFormatter.date(from: currentRes!.startDate) else {return}
                    start = start.addingTimeInterval(3600*3)
                    
                    guard var end = self.dateFormatter.date(from: currentRes!.endDate) else {return}
                    end = end.addingTimeInterval(3600*3)
                    
                    if start <= now && now <= end {
                        self.currentRes = currentRes?.reservationKey! as! String
                    }
                    })
            }
        }
        
        if let chats = chatRoom {
            let ref = Database.database().reference(withPath: "messages/\(chats.userId!)_\(chats.doctorId!)")
            ref.observe(.value, with: {(snapshot) in
                self.messages = []
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot {
                        let message = Message(snapshot: snapshot)
                        self.messages.append(message!)
                    }
                }
                self.messagesCollectionView.reloadData()
                self.messagesCollectionView.scrollToBottom(animated: false)
            })
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if currentRes == "" {
            self.messageInputBar.isUserInteractionEnabled = false
            self.messageInputBar.inputTextView.placeholder = "Şuan Mesaj Yollayamazsın"
        } else {
            reference = Database.database().reference(withPath: "reservations/\(currentRes)")
            reference.observe(.value, with: { snap in
                    let currentRes = ReservationModel(snapshot: snap)
                    var now = Date()
                    now = now.addingTimeInterval(3600*3)
                guard var start = self.dateFormatter.date(from: currentRes!.startDate) else {return}
                    start = start.addingTimeInterval(3600*3)
                    
                guard var end = self.dateFormatter.date(from: currentRes!.endDate) else {return}
                    end = end.addingTimeInterval(3600*3)
                    
                    if start <= now && now <= end {
                        self.messageInputBar.isUserInteractionEnabled = true
                        self.messageInputBar.inputTextView.placeholder = "Mesaj Yolla"
                    }
                    })
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        reference.removeAllObservers()
    }
}

    
    


extension ChatViewController: MessagesDataSource {
    func currentSender() -> SenderType {
        return Sender(senderId: LoginModel.loginUser.userId, displayName: "Sarp")
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messages[indexPath.section]
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
}

extension ChatViewController: MessagesLayoutDelegate, MessagesDisplayDelegate {
    func avatarSize(for message: MessageType, at indexPath: IndexPath,
      in messagesCollectionView: MessagesCollectionView) -> CGSize {
      return .zero
    }
    
    func footerViewSize(for message: MessageType, at indexPath: IndexPath,
      in messagesCollectionView: MessagesCollectionView) -> CGSize {
      return CGSize(width: 0, height: 8)
    }

    func heightForLocation(message: MessageType, at indexPath: IndexPath,
      with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
      return 0
    }
    
    func backgroundColor(for message: MessageType, at indexPath: IndexPath,
      in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .systemGreen : .systemGray
    }
    
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .white : .white
    }

    func shouldDisplayHeader(for message: MessageType, at indexPath: IndexPath,
      in messagesCollectionView: MessagesCollectionView) -> Bool {
      return false
    }

    func messageStyle(for message: MessageType, at indexPath: IndexPath,
      in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
      let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        return .bubbleTail(corner, .pointedEdge)
    }
}

extension ChatViewController: InputBarAccessoryViewDelegate {
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        let ref = Database.database().reference(withPath: "messages/\(chatRoom.userId!)_\(chatRoom.doctorId!)")
        let meetingRef = Database.database().reference(withPath: "meetings/\(chatRoom.userId!)_\(chatRoom.doctorId!)")
        let now = Date()
        let message = Message(content: text, created: now, senderID: LoginModel.loginUser.userId)
        let id = String(currentTimeMillis())
        let refToAdd = ref.child(id) //timestamp
//        let dateFormatter = DateFormatter()
//        dateFormatter.locale = NSLocale.current
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
        let dateFormatter = ISO8601DateFormatter()
//        let dateAndTimeFormatter = DateFormatter()
//        dateAndTimeFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
        
        let time = dateFormatter.string(from: now)
        let newMessage = [
            "message" : "\(message.content)",
            "date" : "\(time)",
            "senderId" : "\(LoginModel.loginUser.userId)"
            ]
        refToAdd.setValue(newMessage)
        
        let meetingLastMessage = meetingRef.child("lastMessage")
        meetingLastMessage.setValue(newMessage)
        inputBar.inputTextView.text = ""
        messagesCollectionView.reloadData()
        messagesCollectionView.scrollToBottom(animated: false)
    }
    
    func currentTimeMillis() -> Int64{
        let nowDouble = NSDate().timeIntervalSince1970
        return Int64(nowDouble*1000)
    }
}
