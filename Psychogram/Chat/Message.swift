//
//  Message.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 26.04.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//


import MessageKit
import FirebaseDatabase
import Firebase

struct Message {
    var content: String
    var created: Date
    var senderID: String
    
    var dictionary: [String: Any] {
        
        return [
            "content": content,
            "created": created,
            "senderID": senderID ]
    }
}

extension Message {
    init?(dictionary: [String: AnyObject]) {
        
        guard let content = dictionary["message"] as? String,
            let created = dictionary["date"] as? String,
            let senderID = dictionary["senderId"] as? String
            else {return nil}
        
        let dateFormatter = ISO8601DateFormatter()
        let datee = dateFormatter.date(from:created)!
        
        self.init(content: content, created: datee, senderID: senderID)
    }
    
    init?(snapshot: DataSnapshot) {
      guard
        let value = snapshot.value as? [String: AnyObject],
        let content = value["message"] as? String,
        let date = value["date"] as? String,
        let sender = value["senderId"] as? String else {
        return nil
      }
        
        let dateFormatter = ISO8601DateFormatter()
        //let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
        let datee = dateFormatter.date(from:date)!
      //self.ref = snapshot.ref
        self.init(content: content, created: datee, senderID: sender)
    }
}

extension Message: MessageType {
    
    var sender: SenderType {
        return Sender(id: senderID, displayName: "x")
    }
    
    var messageId: String {
        return "1"
    }
    
    var sentDate: Date {
        return Date()
    }
    
    var kind: MessageKind {
        return .text(content)
    }
}
