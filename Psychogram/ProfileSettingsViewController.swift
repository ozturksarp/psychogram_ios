//
//  ProfileSettingsViewController.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 4.04.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import UIKit

protocol ProfileSettingsDelegate: class {
    func profileSettings(didEditingFinish controller: ProfileSettingsViewController, about: String, location: String, birthday: String, profilePic: UIImage)
}

class ProfileSettingsViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var aboutUITextView: UITextView!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var profilePhotoIV: UIImageView!
    let datePicker = UIDatePicker()
    
    var about: String!
    var birthday: String!
    var location: String!
    var profilePhoto: UIImage!
    var delegate: ProfileSettingsDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setTextValues()
        setupProfilePhotoIV()
    }
    
    func setTextValues() {
        aboutUITextView.text = about
        birthdayTextField.text = birthday
        locationTextField.text = location
    }
    
    func setupUI() {
        aboutUITextView.layer.borderColor = UIColor.gray.cgColor
        aboutUITextView.layer.borderWidth = 1.0
        aboutUITextView.layer.cornerRadius = 5.0
        
        birthdayTextField.layer.borderColor = UIColor.gray.cgColor
        birthdayTextField.layer.borderWidth = 1.0
        birthdayTextField.layer.cornerRadius  = 5.0
        
        locationTextField.layer.borderColor = UIColor.gray.cgColor
        locationTextField.layer.borderWidth = 1.0
        locationTextField.layer.cornerRadius  = 5.0
        
        updateButton.setTitle("Güncelle", for: .normal)
        updateButton.layer.cornerRadius = 10
        
        datePicker.datePickerMode = .date
        birthdayTextField.inputView = datePicker
        datePicker.addTarget(self, action: #selector(ProfileSettingsViewController.datePickerValueChanged), for: .valueChanged)
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(onClickDoneButton))
        toolBar.setItems([space, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        birthdayTextField.inputAccessoryView = toolBar
        
        profilePhotoIV.image = profilePhoto
    }
    
    func setupProfilePhotoIV() {
        profilePhotoIV.layer.masksToBounds = false
        profilePhotoIV.layer.cornerRadius = profilePhotoIV.frame.height / 2
        profilePhotoIV.clipsToBounds = true
    }
    @IBAction func changePhoto(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else { return }
        dismiss(animated: true, completion: nil)
        profilePhotoIV.image = image
        profilePhoto = image
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        birthdayTextField.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func onClickDoneButton() {
        self.view.endEditing(true)
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }
    
    @IBAction func update(_ sender: Any) {
        self.delegate?.profileSettings(didEditingFinish: self, about: aboutUITextView.text ?? "", location: locationTextField.text ?? "", birthday: birthdayTextField.text ?? "", profilePic: profilePhoto)
    }
}
