//
//  ProfileViewController.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 31.03.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import UIKit
import Firebase

class ProfileViewController: UITableViewController, ProfileSettingsDelegate {
    func profileSettings(didEditingFinish controller: ProfileSettingsViewController, about: String, location: String, birthday: String, profilePic: UIImage) {
        controller.navigationController?.popViewController(animated: true)
        locationLabel.text = location
        aboutLabel.text = about
        birthdayLabel.text = birthday
        currentProfilePhoto = profilePic
        profileIV.image = profilePic
        hasProfilePhoto = true
        tableView.reloadData()
    }

    @IBOutlet weak var profileIV: UIImageView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var starIV: UIImageView!
    @IBOutlet weak var reservationButton: UIButton!
    @IBOutlet weak var namelabel: UILabel!
    @IBOutlet weak var settingsButton: UIBarButtonItem!
    @IBOutlet weak var starsStackView: UIStackView!
    @IBOutlet weak var emailLabel: UILabel!
    
    var baseProfilePhoto = UIImage(named: "profile_photo_sample")
    var currentProfilePhoto: UIImage?
    var hasProfilePhoto = false
    var location: String = "-"
    var birthday: String = "-"
    var email: String = "-"
    var about: String = "-"
    var user: UserModel?
    
    //let ref = Database.database().reference(withPath: "meetings")
    
    // MARK: USER MODELE GORE GELISTIRME YAPILACAK DAHA SONRA
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProfilePhotoIV()
        setupHeaderUI()
        configureView()
        configureButton()
    }
    
    func setupHeaderUI() {
        headerView.layer.backgroundColor = UIColor(displayP3Red: 245/255, green: 245/255, blue: 245/255, alpha: 1.0).cgColor
        headerView.layer.cornerRadius = 10.0
        headerView.layer.shadowColor = UIColor(displayP3Red: 192/255, green: 192/255, blue: 192/255, alpha: 1.0).cgColor
        headerView.layer.shadowOpacity = 1
        headerView.layer.shadowOffset = .zero
        headerView.layer.shadowRadius = 10
    }
    
    func configureView() {
        if LoginModel.loginUser.userId != user!.userId {
            self.navigationItem.rightBarButtonItem = nil
            self.navigationItem.title = "\(user!.username!)"
        } else {
            reservationButton.isHidden = true
            starsStackView.isHidden = true
        }
        aboutLabel.text = user?.description
        locationLabel.text = user?.location
        birthdayLabel.text = user?.birthday
        namelabel.text = "\(user!.name! ?? "") \(user!.surname! ?? "")"
        emailLabel.text = user?.email
    }
    
    func setupProfilePhotoIV() {
        if let profilePic = user?.profilePhotoSource {
            let url = URL(string: profilePic)
            profileIV.kf.setImage(with: url)
        } else {
            currentProfilePhoto = baseProfilePhoto
            profileIV.image = baseProfilePhoto
        }
        profileIV.layer.masksToBounds = false
        profileIV.layer.cornerRadius = profileIV.frame.height / 2
        profileIV.clipsToBounds = true
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.section == 0 && indexPath.row == 0 {
//            return 200.0
//        }
        tableView.estimatedRowHeight = 250
        return UITableView.automaticDimension
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "settings" {
            let settings = segue.destination as! ProfileSettingsViewController
            settings.about = aboutLabel?.text ?? ""
            settings.location = locationLabel?.text ?? ""
            settings.birthday = birthdayLabel?.text ?? ""
            settings.profilePhoto = profileIV.image
            settings.delegate = self
        } else if segue.identifier == "ChatScreen" {
//            let ref = Database.database().reference(withPath: "meetings")
//            let meeting = "\(LoginModel.loginUser.userId)_\(user!.userId!)"
//            let chatVC = segue.destination as! ChatViewController
//            chatVC.otherUserId = user!.userId!
//            ref.observe(.value, with: {(snapshot) in
//                print("LOLLLL")
//                if snapshot.hasChild(meeting) {
//                    chatVC.chatRoom = meeting
//                }
//            })
        } else if segue.identifier == "Reservation" {
            let vc = segue.destination as! ReservationViewController
            vc.doctorId = user?.userId
        }
    }
    
    @IBAction func showSettings(_ sender: Any) {
        performSegue(withIdentifier: "settings", sender: sender)
    }
    
    func configureButton() {
        reservationButton.layer.cornerRadius = 8
    }
    
}
