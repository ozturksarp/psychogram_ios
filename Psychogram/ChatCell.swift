//
//  ChatCell.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 30.04.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {
    
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var lastMessageLabel: UILabel!
    @IBOutlet weak var userPhotoIV: UIImageView!
    

}
