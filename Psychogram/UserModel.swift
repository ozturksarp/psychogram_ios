//
//  UserModel.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 8.04.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import Foundation
import Firebase

struct UserModel {
    //let ref: DatabaseReference?
    let key: String
    //let username: String
    
    var userId: String?
    var birthday: String!
    var description: String!
    var email: String!
    var location: String!
    var name: String!
    var surname: String!
    var phone: String!
    var profilePhotoSource: String!
    var role: String!
    var username: String!
    
    init(username: String, key: String = "") {
        //self.ref = nil
        self.key = key
        self.username = username
    }
    
    init(loginUser: LoginModel) {
        self.username = loginUser.username
        self.birthday = loginUser.birthday
          self.description = loginUser.description
          self.email = loginUser.email
          self.location = loginUser.location
          self.name = loginUser.name
          self.surname = loginUser.surname
          self.phone = loginUser.phone
          self.profilePhotoSource = loginUser.profilePhotoSource
          self.role = loginUser.role
          self.username = loginUser.username
        self.key = ""
        //self.ref = ""
    }
    
    init?(snapshot: DataSnapshot) {
      guard
        let value = snapshot.value as? [String: AnyObject],
        let birthday = value["birthday"] as? String,
        let desc = value["description"] as? String,
        let email = value["email"] as? String,
        let location = value["location"] as? String,
        let name = value["name"] as? String,
        let surname = value["surname"] as? String,
        let phone = value["phone"] as? String,
        let profilePhotoSource = value["profilePictureSource"] as? String,
        let role = value["role"] as? String,
        let username = value["username"] as? String else {
        return nil
      }
        
      //self.ref = snapshot.ref
      self.key = snapshot.key
      self.username = username
      self.birthday = birthday
        self.description = desc
        self.email = email
        self.location = location
        self.name = name
        self.surname = surname
        self.phone = phone
        self.profilePhotoSource = profilePhotoSource
        self.role = role
        self.username = username
        
    }
    
//    func toAnyObject() -> Any {
//      return [
//        "name": name,
//        "addedByUser": addedByUser,
//        "completed": completed
//      ]
//    }
}
