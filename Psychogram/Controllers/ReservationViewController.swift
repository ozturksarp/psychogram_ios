//
//  ReservationViewController.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 15.05.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import UIKit
import Firebase

class ReservationViewController: UIViewController {

    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var endDateTextField: UITextField!
    @IBOutlet weak var descTextField: UITextField!
    @IBOutlet weak var reservationButton: UIButton!
    var doctorId: String?
    let startDatePicker = UIDatePicker()
    let endDatePicker = UIDatePicker()
    let ref = Database.database().reference(withPath: "reservations")
    var startDate: String?
    var endDate: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        reservationButton.layer.cornerRadius = 8
        startDatePicker.datePickerMode = .dateAndTime
        startDatePicker.addTarget(self, action: #selector(ReservationViewController.startDatePickerValueChanged), for: .valueChanged)
        endDatePicker.datePickerMode = .dateAndTime
        endDatePicker.addTarget(self, action: #selector(ReservationViewController.endDatePickerValueChanged), for: .valueChanged)
        startDateTextField.inputView = startDatePicker
        endDateTextField.inputView = endDatePicker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(onClickDoneButton))
        toolBar.setItems([space, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        startDateTextField.inputAccessoryView = toolBar
        endDateTextField.inputAccessoryView = toolBar
    }
    
    @objc func startDatePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        let dateAndTimeFormatter = ISO8601DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        startDateTextField.text = dateFormatter.string(from: sender.date)
        startDate = dateAndTimeFormatter.string(from: sender.date)
    }
    
    @objc func endDatePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        let dateAndTimeFormatter = ISO8601DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        endDateTextField.text = dateFormatter.string(from: sender.date)
        endDate = dateAndTimeFormatter.string(from: sender.date)
    }
    
    @objc func onClickDoneButton() {
        self.view.endEditing(true)
    }
    
    @IBAction func makeReservation(_ sender: Any) {
        // fix later
        // mesajı yolla backende
        if startDateTextField.text!.count > 0 && endDateTextField.text!.count > 0 {
            let newReservation = [
                "allDay" : false,
                "doctorId": "\(doctorId!)",
                "endDate": "\(endDate!)",
                "startDate": "\(startDate!)",
                "isEnded": false,
                "flag": false,
                "title" : "Rezervasyon",
                "userId" : "\(LoginModel.loginUser.userId)"
            ] as [String : Any]
            
            let refReservationKey = ref.childByAutoId()
            let res = ref.child(refReservationKey.key!)
            res.setValue(newReservation)
            self.navigationController?.popViewController(animated: true)
        }
    }
}
