//
//  AllUsersViewController.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 22.04.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Kingfisher

class AllUsersViewController: UITableViewController {
    
    let ref = Database.database().reference(withPath: "users")
    let reservationRef = Database.database().reference(withPath: "reservations")
    let meetingRef = Database.database().reference(withPath: "meetings")
    var allUsers: [UserModel] = []
    var reservations: [ReservationModel] = []
    var images: [UIImage] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if LoginModel.loginUser.role == "DOCTOR" {
            navigationItem.title = "Rezervasyonlar"
            reservationRef.observe(.value, with: { snapshot in
                var res: [ReservationModel] = []
                for child in snapshot.children {
                    let key = (child as AnyObject).key as String
                    if let snapshot = child as? DataSnapshot,
                        let reservation = ReservationModel(snapshot: snapshot) {
                        if reservation.doctorId == LoginModel.loginUser.userId {
                            res.append(reservation)
                        }
                    }
                }
                self.reservations = res
                self.tableView.reloadData()
            })
        } else if LoginModel.loginUser.role == "PATIENT" {
            navigationItem.title = "Psikologlar"
            ref.observe(.value, with: { snapshot in
                var users: [UserModel] = []
                for child in snapshot.children {
                    let key = (child as AnyObject).key as String
                    if let snapshot = child as? DataSnapshot,
                        var user = UserModel(snapshot: snapshot) {
                        user.userId = key
                        if user.role == "DOCTOR" && user.userId != LoginModel.loginUser.userId {
                            users.append(user)
                        }
                    }
                }
                self.allUsers = users
                self.tableView.reloadData()
            })
        }
        
        tableView.tableFooterView = UIView()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if LoginModel.loginUser.role == "DOCTOR" {
            return reservations.count
        } else if LoginModel.loginUser.role == "PATIENT" {
            return allUsers.count
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if LoginModel.loginUser.role == "DOCTOR" {
            let res = reservations[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReservationTableCell", for: indexPath) as! ReservationCell
            cell.descLabel.text = "\(res.userId!) talep etti."
            cell.titleLabel.text = res.resTitle
            cell.profilePhotoIV.image = UIImage(named: "profile_user_sample")
            cell.requesterId = res.userId
            cell.reservationKey = res.reservationKey
            
            let user = res.userId
            let child = ref.child("\(user!)")
            var username = ""
            child.observeSingleEvent(of: .value, with: {snapshot in
                if let snap = snapshot as? DataSnapshot {
                    let user = UserModel(snapshot: snap)
                    username = user!.username
                    cell.descLabel.text = "\(username) talep etti."
                    let url = URL(string: user!.profilePhotoSource)
                    cell.profilePhotoIV.kf.setImage(with: url)
                    cell.profilePhotoIV.layer.cornerRadius = 32
                    cell.profilePhotoIV.layer.masksToBounds = true
                }
            })
            
            let reservationsOfMeeting = meetingRef.child("\(res.userId!)_\(LoginModel.loginUser.userId)/reservations")
            reservationsOfMeeting.observeSingleEvent(of: .value, with: {snapshot in
                for child in snapshot.children {
                    let val = (child as AnyObject).value as String
                    if val == res.reservationKey {
                        cell.acceptButton.isEnabled = false
                        cell.acceptButton.backgroundColor = .systemGreen
                        cell.acceptButton.setTitle("Onayladın", for: .normal)
                    }
                }
            })
            
            return cell
        } else {
            let user = allUsers[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
            cell.profilePhotoImageView.layer.cornerRadius = 32
            cell.profilePhotoImageView.layer.masksToBounds = true
            cell.profilePhotoImageView.image = UIImage(named: "profile_user_sample")
            let url = URL(string: allUsers[indexPath.row].profilePhotoSource)
            cell.profilePhotoImageView.kf.setImage(with: url)
            cell.usernameLabel.text = user.username
            cell.locationLabel.text = user.location
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathForSelectedRow {
            guard let destinationVC = segue.destination as? ProfileViewController else {return}
            let selectedRow = indexPath.row
            destinationVC.user = allUsers[selectedRow]
        }
    }
}
