//
//  AllChatsViewController.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 29.04.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import UIKit
import Firebase

class AllChatsViewController: UITableViewController {
    
    let ref = Database.database().reference(withPath: "meetings")
    let reference = Database.database().reference(withPath: "users")
    var chats: [Chat] = []
    var contactedUsernames: [String] = []
    
    var dateFormatter = ISO8601DateFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Sohbetler"
        
        if LoginModel.loginUser.role == "DOCTOR" {
            ref.observe(.value, with: {(snapshot) in
                self.chats = []
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot {
                        if snapshot.key.contains("\(LoginModel.loginUser.userId)") {
                            let chat = Chat(snapshot: snapshot)
                            
                            if !(chat!.reservationKeys.isEmpty) {
                                for res in chat!.reservationKeys {
                                    let reference = Database.database().reference(withPath: "reservations/\(res)")
                                    reference.observeSingleEvent(of: .value, with: { snap in
                                        print("LOAD")
                                        let currentRes = ReservationModel(snapshot: snap)
                                        print(currentRes?.reservationKey)
                                        var now = Date()
                                        now = now.addingTimeInterval(3600*3)
                                        guard var start = self.dateFormatter.date(from: currentRes!.startDate) else {return}
                                        start = start.addingTimeInterval(3600*3)
                                        
                                        guard var end = self.dateFormatter.date(from: currentRes!.endDate) else {return}
                                        end = end.addingTimeInterval(3600*3)
                                        
                                        if start <= now && now <= end {
                                            self.chats.append(chat!)
                                            self.tableView.reloadData()
                                        }
                                    })
                                }
                            }
                        }
                    }
                }
                //self.chats = c
                self.tableView.reloadData()
            })
        } else {
            let query = ref.queryOrdered(byChild: "userId").queryStarting(atValue: "\(LoginModel.loginUser.userId)").queryEnding(atValue: LoginModel.loginUser.userId+"\u{f8ff}")

            query.observe(.value, with: {(snapshot) in
                var c: [Chat] = []
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot,
                        let chat = Chat(snapshot: snapshot) {
                        c.append(chat)
                    }
                }
                self.chats = c
                self.tableView.reloadData()
            })
            
            self.tableView.reloadData()
        }
        
        tableView.reloadData()
        
    }
    
    func getUserById(userId: String) -> String {
        let reference = Database.database().reference(withPath: "users/\(userId)")
        var username = userId

        reference.observeSingleEvent(of: .value, with: {(snapshot) in
            if let value = snapshot.value as? [String: AnyObject] {
               username = value["username"] as! String
            }
        })
        return username
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chats.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! ChatCell
        if LoginModel.loginUser.role == "DOCTOR" {
            let user = chats[indexPath.row].userId
            let child = reference.child("\(user!)")
            var username = ""
            child.observeSingleEvent(of: .value, with: {snapshot in
                if let snap = snapshot as? DataSnapshot {
                    let user = UserModel(snapshot: snap)
                    username = user!.username
                    cell.userLabel.text = username
                    let url = URL(string: user!.profilePhotoSource)
                    cell.userPhotoIV.kf.setImage(with: url)
                    cell.userPhotoIV.layer.cornerRadius = 32
                    cell.userPhotoIV.layer.masksToBounds = true
                }
            })
        } else if LoginModel.loginUser.role == "PATIENT" {
            let user = chats[indexPath.row].doctorId
            let child = reference.child("\(user!)")
            var username = ""
            child.observeSingleEvent(of: .value, with: {snapshot in
                if let snap = snapshot as? DataSnapshot {
                    let user = UserModel(snapshot: snap)
                    username = user!.username
                    cell.userLabel.text = username
                    let url = URL(string: user!.profilePhotoSource)
                    cell.userPhotoIV.kf.setImage(with: url)
                    cell.userPhotoIV.layer.cornerRadius = 32
                    cell.userPhotoIV.layer.masksToBounds = true
                }
            })
        }
        
        let user = chats[indexPath.row].lastMessage.senderID
        let child = reference.child("\(user)")
        var username = ""
        child.observeSingleEvent(of: .value, with: {snapshot in
            if let snap = snapshot as? DataSnapshot {
                let user = UserModel(snapshot: snap)
                username = user!.username
                cell.lastMessageLabel.text = "\(username): \(self.chats[indexPath.row].lastMessage.content)"
            }
        })
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathForSelectedRow {
            guard let vc = segue.destination as? ChatViewController else { return }
            vc.chatRoom = chats[indexPath.row]
        }
    }
}
