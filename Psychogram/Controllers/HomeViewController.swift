//
//  HomeViewController.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 31.03.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import UIKit
import FirebaseDatabase

class HomeViewController: UIViewController {

    @IBOutlet weak var welcomeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: false)
        let def = UserDefaults.standard.bool(forKey: "userSignedIn")
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OwnProfile" {
            var currentUser = UserModel(loginUser: LoginModel.loginUser)
            currentUser.userId = LoginModel.loginUser.userId
            guard let destinationVC = segue.destination as? ProfileViewController else {return}
            destinationVC.user = currentUser
        }
    }

}
