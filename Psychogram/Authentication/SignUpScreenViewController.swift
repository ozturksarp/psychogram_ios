//
//  SignUpScreenViewController.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 26.03.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

protocol SignUpViewControllerDelegate: class {
    func signUpViewController(_ controller: SignUpScreenViewController, email: String, password: String)
}


class SignUpScreenViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var userTypeSegment: UISegmentedControl!
    @IBOutlet weak var signUpButton: UIButton!
    
    weak var delegate: SignUpViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupButton()
        usernameTextField.delegate = self
        passwordTextField.delegate = self
        emailTextField.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func setupButton() {
        signUpButton.layer.cornerRadius = 25
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func signUp(_ sender: Any) {
        guard let username = usernameTextField.text else { return }
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        
        Auth.auth().createUser(withEmail: email, password: password) {(authResult, error) in
            if let user = authResult?.user {
                self.signUpUser(with: user.uid)
                self.delegate?.signUpViewController(self, email: email, password: password)
                
            } else {
                print("error")
            }
        }
    }
    
    func signUpUser(with id: String) {
        let ref = Database.database().reference(withPath: "users")
        let refToAdd = ref.child(id)
        var role = ""
        
        if userTypeSegment.selectedSegmentIndex == 0 {
            role = "DOCTOR"
        } else {
            role = "PATIENT"
        }
        
        let newUser = [
          "birthday" : "",
          "description" : "",
          "email" : "\(emailTextField!.text!)",
          "location" : "",
          "name" : "\(nameTextField!.text!)",
          "phone" : "",
          "profilePictureSource" : "",
          "role" : "\(role)",
          "surname" : "\(surnameTextField!.text!)",
          "username" : "\(usernameTextField!.text!)"
        ]
        refToAdd.setValue(newUser)
    }
}
