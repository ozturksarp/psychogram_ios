//
//  LoginScreenViewController.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 24.03.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginScreenViewController: UIViewController, UITextFieldDelegate, SignUpViewControllerDelegate {
    func signUpViewController(_ controller: SignUpScreenViewController, email: String, password: String) {
        controller.dismiss(animated: true, completion: nil)
        usernameTextField.text = email
        passwordTextLabel.text = password
    }
    
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextLabel: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var facebookLoginButtton: UIImageView!
    @IBOutlet weak var googleLoginButton: UIImageView!
    @IBOutlet weak var noAccountButton: UIButton!
    var handle: AuthStateDidChangeListenerHandle?
    let userDefault = UserDefaults.standard
    let launchedBefore = UserDefaults.standard.bool(forKey: "userSignedIn")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: true)
        usernameTextField.delegate = self
        passwordTextLabel.delegate = self
        setupButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let isSignedIn = UserDefaults.standard.bool(forKey: "userSignedIn")
        if isSignedIn {
            let id = UserDefaults.standard.string(forKey: "userID")
            LoginModel.loginUser.setCurrentUserId(with: id!)
            LoginModel.loginUser.fetchLoggedInUser()
            performSegue(withIdentifier: "AutoLogin", sender: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
//        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
//          // ...
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //Auth.auth().removeStateDidChangeListener(handle!)
    }
    
    func setupButton() {
        loginButton.layer.cornerRadius = 25
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func login(_ sender: Any) {
        guard let password = passwordTextLabel.text else { return }
        guard let email = usernameTextField.text else {return }

        Auth.auth().signIn(withEmail: email, password: password) { user, error in
          if let error = error, user == nil {
            let alert = UIAlertController(title: "Giriş Başarısız",
                                          message: error.localizedDescription,
                                          preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Tamam", style: .default))
            self.present(alert, animated: true, completion: nil)
          } else {
            let userId = FirebaseAuth.Auth.auth().currentUser!.uid
            LoginModel.loginUser.setCurrentUserId(with: userId)
            LoginModel.loginUser.fetchLoggedInUser()
            self.userDefault.set(true, forKey: "userSignedIn")
            self.userDefault.set(userId, forKey: "userID")
            self.userDefault.synchronize()
            self.performSegue(withIdentifier: "ShowHome", sender: nil)
            }
        }
    }
    @IBAction func signUp(_ sender: Any) {
        performSegue(withIdentifier: "ShowSignUp", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowSignUp" {
            let controller = segue.destination as! SignUpScreenViewController
            controller.delegate = self
        }
    }
    
}
