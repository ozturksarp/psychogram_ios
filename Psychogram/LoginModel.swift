//
//  LoginModel.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 25.04.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import Foundation
import Firebase

class LoginModel {
    
    static let loginUser = LoginModel()
    let ref = Database.database().reference(withPath: "users")
    
    
    var userId: String = ""
    var birthday: String!
    var description: String!
    var email: String!
    var location: String!
    var name: String!
    var surname: String!
    var phone: String!
    var profilePhotoSource: String!
    var role: String!
    var username: String!
    
    private init(){}
    
    func fetchLoggedInUser() {
        let ref = Database.database().reference(withPath: "users/\(userId)")
        
        ref.observe(.value, with: { snapshot in
            guard let value = snapshot.value as? [String: AnyObject] else {
                return
            }
            self.birthday = value["birthday"] as? String
            self.description = value["description"] as? String
            self.email = value["email"] as? String
            self.location = value["location"] as? String
            self.name = value["name"] as? String
            self.surname = value["surname"] as? String
            self.phone = value["phone"] as? String
            self.profilePhotoSource = value["profilePictureSource"] as? String
            self.role = value["role"] as? String
            self.username = value["username"] as? String
        })
    }
    
    func setCurrentUserId(with id: String) {
        userId = id
    }
}
