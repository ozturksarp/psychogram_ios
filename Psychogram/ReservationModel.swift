//
//  ReservationModel.swift
//  Psychogram
//
//  Created by Sarp Ozturk on 15.05.2020.
//  Copyright © 2020 Sarp Ozturk. All rights reserved.
//

import Foundation
import Firebase

struct ReservationModel {
    var userId: String?
    var doctorId: String!
    var endDate: String!
    var startDate: String!
    var resTitle: String!
    var reservationKey: String?
    
    init?(snapshot: DataSnapshot) {
      guard
        let value = snapshot.value as? [String: AnyObject],
        let key = snapshot.key as? String,
        let user = value["userId"] as? String,
        let doctor = value["doctorId"] as? String,
        let end = value["endDate"] as? String,
        let title = value["title"] as? String,
        let start = value["startDate"] as? String else {
        return nil
      }
        
        self.userId = user
        self.doctorId = doctor
        self.endDate = end
        self.startDate = start
        self.resTitle = title
        self.reservationKey = key
    }
}
